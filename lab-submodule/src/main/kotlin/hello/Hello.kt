package hello

import java.util.*
import java.util.Collections.swap

fun main() {
    println("Sub module hello")
}

class HeapQueueDouble(maxSize: Int = 1000) : Queue<Double> {
    val backupArray = Array<Double>(maxSize + 1) { i -> 0.0 }
    var _size = 0;

    override fun contains(element: Double?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addAll(elements: Collection<Double>): Boolean {
        elements.forEach({ ele -> add(ele) })
        return true;
    }

    override fun clear() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun element(): Double {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isEmpty(): Boolean {
        return size == 0;
    }

    override fun remove(): Double {
        val element = backupArray[1]
        backupArray[1] = backupArray[--_size]
        bubbleDown(1)
        return element;
    }

    override val size: Int
        get() = _size;

    override fun containsAll(elements: Collection<Double>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun iterator(): MutableIterator<Double> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun remove(element: Double?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeAll(elements: Collection<Double>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun add(element: Double?): Boolean {
        backupArray[++_size] = element ?: 0.0
        bubbleUp(_size)
        return true;
    }

    fun bubbleUp(ind: Int) {
        val par = parent(ind);
        if (par < 1) return;

        if (backupArray[par] > backupArray[ind]) {
            swap(par, ind)
            bubbleUp(par)
        }
    }

    fun bubbleDown(ind: Int) {
        val leftChild = leftChild(ind)
        if (leftChild > size) return
        val rightChild = rightChild(ind)
        val minChild =
            if (rightChild > size || backupArray[leftChild] < backupArray[rightChild]) leftChild else rightChild

        if (backupArray[ind] > backupArray[minChild]) {
            swap(ind, minChild)
            bubbleDown(minChild)
        }
    }

    fun swap(a: Int, b: Int) {
        val tmp = backupArray[a];
        backupArray[a] = backupArray[b];
        backupArray[b] = tmp;
    }

    override fun offer(e: Double?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retainAll(elements: Collection<Double>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun peek(): Double {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun poll(): Double {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun toString(): String {
        return "HeapQueueDouble(backupArray=${backupArray.take(size + 1)}, size=$size)"
    }

    companion object {
        fun leftChild(parent: Int): Int = parent * 2
        fun rightChild(parent: Int): Int = parent * 2 + 1
        fun parent(child: Int): Int = child / 2

        fun buildHeap(arr: Array<Double>): Array<Double> {
            val queue = HeapQueueDouble(arr.size)
            queue.addAll(arr)
            val sorted = Array<Double>(arr.size, { i -> 0.0 });
            for (i in 0..arr.size) {
                arr[i] = queue.remove()
            }
            return sorted
        }
    }
}
