package hello

import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test

class HeapQueueDoubleTest {
    val empty = HeapQueueDouble();

    @Test
    fun toStringTest() {
        Assert.assertEquals(
            "HeapQueueDouble(backupArray=[], size=0)",
            empty.toString()
        )
    }

    @Test
    fun isEmpty() {
        Assert.assertTrue(empty.isEmpty())
        Assert.assertEquals(0, empty.size)
    }

    @Test
    fun insert() {
        var heap = HeapQueueDouble();
        heap.add(10.0)
        heap.add(20.0)
        heap.add(0.0)
        heap.add(-10.0)
        heap.add(50.0)
        heap.add(60.0)

        Assert.assertEquals(
            "HeapQueueDouble(backupArray=[0.0, -10.0, 0.0, 10.0, 20.0, 50.0, 60.0], size=6)",
            heap.toString()
        )

        heap.add(100.0)

        Assert.assertEquals(
            "HeapQueueDouble(backupArray=[0.0, -10.0, 0.0, 10.0, 20.0, 50.0, 60.0, 100.0], size=7)",
            heap.toString()
        )

        heap.add(-40.0);


        Assert.assertEquals(
            "HeapQueueDouble(backupArray=[0.0, -40.0, -10.0, 10.0, 0.0, 50.0, 60.0, 100.0, 20.0], size=8)",
            heap.toString()
        )

        Assert.assertEquals(-40.0, heap.remove(), 0.1)
        Assert.assertEquals(-10.0, heap.remove(), 0.1)

        Assert.assertEquals(
            "HeapQueueDouble(backupArray=[0.0, 0.0, 20.0, 10.0, 100.0, 50.0, 60.0], size=8)",
            heap.toString()
        )

        /*
                       0
                 20         10
             100     50   60
        */
    }
}
