package cz.fit.cvut.bi.kot

import java.lang.IllegalStateException

interface Elem {
    val width: Int
        get() = content[0].length
    val height: Int
        get() = content.size
    val content: Array<String>
}

abstract class AElem : Elem {
    override fun toString() = content.joinToString(separator = "\n")
}

fun Elem.above(other: Elem): Elem {
    require(width == other.width) { "nesouhlasi sirka" }
    return BasicElem(content + other.content)
}

fun Elem.widenR(w: Int): Elem {
    require(w >= width)
    return if (w == width) this
    else this.beside(EmptyElem(w - width, height))

}

fun Elem.beside(other: Elem): Elem {
    require(height == other.height) { "nesouhlasi vyska" }
    return BasicElem(Array(height) { ind -> content[ind] + other.content[ind] })
}

open class BasicElem(override val content: Array<String>) : AElem() {
    init {
        require(this.content.all { it.length == content[0].length }) { "neni obdelnikove" }
    }
}

open class CharElem(ch: Char, w: Int, h: Int) : BasicElem(Array(h) { ch.toString().repeat(w) })

class EmptyElem(w: Int, h: Int) : CharElem(' ', w, h)

tailrec fun spiral(s: Int = 12, i: Int = 1, inp: Elem, c: Char = '*'): Elem {
    if (i > s) {
        return inp
    }
    if (i == 1) {
        return spiral(s, i + 1, CharElem(c, 1, 1), c)
    }

    return when (i % 4) {
        1 -> spiral(s, i + 1, CharElem(c, 1, i).beside(EmptyElem(i - 1, 1).above(inp)), c)
        2 -> spiral(s, i + 1, CharElem(c, i, 1).above(inp.beside(EmptyElem(1, i - 1))), c)
        3 -> spiral(s, i + 1, inp.above(EmptyElem(i - 1, 1)).beside(CharElem(c, 1, i)), c)
        0 -> spiral(s, i + 1, EmptyElem(1, i - 1).beside(inp).above(CharElem(c, i, 1)), c)
        else -> throw IllegalStateException()
    }
}

fun main() {
    println(spiral(12, 1, EmptyElem(0, 0)))
}
